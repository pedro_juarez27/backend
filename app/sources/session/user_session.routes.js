const express = require('express');

const api = express.Router();

const controller = require('./user_session.controller');


api.get('/list', controller.getList);
api.put('/update-socket', controller.updateSocket);
api.post('/register', controller.register);

module.exports = api;